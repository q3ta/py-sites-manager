#Проект для управления сайтами на ubuntu.
###PYSM - python sites manager.

##Установка:
sudo mkdir -p /app && cd /app  
git clone git@bitbucket.org:q3ta/py-sites-manager.git  
echo "alias pysm='sudo python /app/py-sites-manager/sites-manager.py'" >> ~/.bash_aliases  

##Запуск:
sudo pysm
