#-*- coding:utf-8 -*-

import os
import signal
import time
import colorama
import sys

def clear_terminal():
    os.system(u'clear')

def get_hosts():
    data = open(ur'/etc/hosts')
    result = [map(lambda x: x.strip().decode(u'utf-8'), row.split(u'\t'))[1] for row in data if len(row.strip()) > 0]
    data.close()
    return result
    

while True:
    if not os.geteuid() == 0:
        print u'{}You must be root to run this application, please use sudo and try again.{}'.format(colorama.Fore.RED, colorama.Fore.RESET)
        sys.exit()
    
    clear_terminal()

    print u'{1}Commands:{0} \n\t{2}a host_name{0} - add new hostname.\n\t{2}d host_name{0} - delete hostname.\n\t{2}r current_host_name new_host_name{0} - rename hostname.\n\t{2}e{0} - exit.'.format(colorama.Fore.RESET, colorama.Fore.YELLOW, colorama.Fore.CYAN)

    
    print u'{}List of hosts:{}'.format(colorama.Fore.YELLOW, colorama.Fore.RESET)
    hosts = get_hosts()
    for host in hosts:
        print u'\t' + host
    
    command = raw_input(u'{}Enter the command: {}'.format(colorama.Fore.YELLOW, colorama.Fore.RESET).encode(u'utf-8')).decode(u'utf-8').split(u' ')
    
    if command[0] not in ('a', 'd', 'r', 'e'):
        continue

    if command[0] ==  u'e':
        sys.exit()
    
    if command[0] in (u'd', u'r'):
        if(command[1] not in hosts):
            continue
        
        os.system(u'rm -r /var/www/{}'.format(command[1]))
        
        hosts.remove(command[1])
        f = open(ur'/etc/hosts', u'w')
        for host in hosts:
            f.write(u'127.0.0.1\t{}\n'.format(host))
        f.close()
        
        os.system(u'rm /etc/apache2/sites-available/{}.conf'.format(u'.'.join(command[1].split('.')[0:-1])))
        os.system(u'rm /etc/apache2/sites-enabled/{}.conf'.format(u'.'.join(command[1].split('.')[0:-1])))
        
    
    if command[0] == u'r':
        command[0] = u'a'
        command[1] = command[2]
    
    if command[0] == u'a':
        if command[1] in hosts:
            continue
        
        os.system(u'mkdir /var/www/{}'.format(command[1]))
        os.system(u'chmod 777 /var/www/{}'.format(command[1]))
        
        f = open(ur'/etc/hosts', u'a')
        f.write(u'127.0.0.1\t{}\n'.format(command[1]))
        f.close()
        
        f = open(ur'/etc/apache2/sites-available/{}.conf'.format(u'.'.join(command[1].split('.')[0:-1])), u'w')
        f.write(u'<VirtualHost *:80>\n\tServerName {0}\n\tDocumentRoot /var/www/{0}\n\t<Directory /var/www/{0}>\n\t\tAllowOverride All\n\t</Directory>\n</VirtualHost>'.format(command[1]))
        f.close()
        
        os.system(u'ln /etc/apache2/sites-available/{}.conf /etc/apache2/sites-enabled/'.format(u'.'.join(command[1].split('.')[0:-1])))
    
    os.system(u'service apache2 restart')
